This application is a qualtrics plugin for capturing body image data.  It will show an image of a person that varies on two axis, amount of fat and amount of muscle.

As the user moves their mouse around the image, they get different combinations of the two axis.  When they are happy with the image, they click their mouse button to fix it in place.

This works as a standalone website for demonstration purposes but will also read information from a qualtrics survey and pass information back.  From qualtrics, the plugin will take:

  * age
  * gender

And use that information to choose which image to show.

It will send back to qualtrics:

  * fat score
  * muscle score
  * an event when the user is finished
  * how long since first showing the data it took for the user to choose their image (TODO)
  * how many times the user changed their mind (TODO)

See "Connecting to Qualtrics" for more information

## Live instance
A live instance of the application is publically available at https://bodyimage.herokuapp.com

## Connecting to Qualtrics

To connect to the plugin, the qualtrics survey should include, at the point were the plugin needs to be viewed, a "text/graphics" question and then replace the contents of that question (via the HTML view) with an iframe tag like so:

~~~~~
<iframe frameborder ="0" 
        height      ="PREFERRED_HEIGHT" 
        width       ="PREFERRED_WIDTH"
        id           ="plugin frame" 
        scrolling    ="yes" 
        src          ="https://URLOFPLUGIN/?data=${e://Field/DATA}" >
</iframe>
~~~~~
Note that `PREFERRED_WIDTH`, `PREFERRED_HEIGHT`, `URLOFPLUGIN` and (maybe) `DATA` will need to be replaced with appropriate values.

They key is the src, which should be the correct url for the plugin you want to use, and the field/data you might want to send across.  

### Sending data from qualtrics to the plugin
Data is sent through query parameters `age` and `gender`.  These accept the following values:

  * `age`: 14 (default) or 17
  * `gender`: Female (default) or Male
  * `muscAs`: The key to use when returning selected muscle percentage.  Can be any string of characters, defaults to "musc".
  * `fatAs`: The key to use when returning selected fat percentage. Can be any string of characters, defaults to "fat".

These can be hard coded or can be pulled from a previous question answer.  If you want to pull the values from embedded data (say the fields "a" and "g") you will construct a URL like

~~~~~
https://URLOFPLUGIN/?age=${e://Field/a}&gender=${e://Field/a}
~~~~~

If you wish to pull these values from the answer to a previous question, you should used piped text expressions like (say the questions were Q1 and Q2)

~~~~~
https://URLOFPLUGIN/?age=${q://QID1/ChoiceGroup/SelectedChoices}&gender=${q://QID2/ChoiceGroup/SelectedChoices}
~~~~~

The two key setting alternatives will only be needed if you are using the plugin more than once in a single experiment.  You will then need multiple embedded data fields to accept the return data and these will need different names.  If you can't tell the plugin what names you have chosen (and thus what keys to return the data under), you can't capture more than one data point.

### Recieving data from the plugin

TODO

## Authors
  * Matthew Roberts
  * Kevin Brooks
  * Deborah Mitchison
  * Ian Stephen
  * Tianqi (Mia) Zhang
  * Victoria Utaree Bashu

## Use

This plugin is open-sourced under the Mozilla public licence and thus there are very few restriction on its use.  As an academic project the authors ask that when the code or the public instance are used that citation (in the case of academic use) or donation (in the case of for-profit use) be considered.
