<?php
error_log("referrer recorded in PHP as");
error_log($_SERVER['HTTP_REFERER']);

$gender = $_GET["gender"] ?? "female";
$age    = $_GET["age"] ?? "14";
$muscAs = $_GET['muscAs'] ?? 'musc';
$fatAs  = $_GET['fatAs'] ?? 'fat';
$idAs   = $_GET['idAs'] ?? "anon";
$style  = $_GET['style'] ?? "slider"; // when this is set to "slider" you will get a slider visible and mouse movement will not control the image.  When set to anything else, the slider is hidden and mouse/touch movement/clicking is used to set the image.

// enumerate accepted values
switch ($gender) {
	case "f":
	case "female":
	case "F":
	case "Female":
	  $gender = "f";
	  break;
	case "m":
	case "male":
	case "M":
	case "Male":
	  $gender = "m";
	  break;
}

// adjust for current crop of images we have
if ($idAs == 'anon'){
	$fatMin   = ($gender == "m") ? "-100": "-33";
	$fatMax   = ($gender == "m") ? "100": "99";
	$fatStep  = ($gender == "m") ? "50": "33";
	$muscMin  = "0";
	$muscMax  = "100";
	$muscStep = "25";
} else {
	$fatMin = "1";
	$fatMax = "13";
	$fatStep = "1";
	$muscMin = "6";
	$muscMax = "6";
	$muscStep = "1";
	$gender = "_";
	$age = "_";
}

?>
<html>
	<head>
		<style>
			.slider {
			  -webkit-appearance: none;
			  width: 100%;
			  height: 25px;
			  background: #d3d3d3;
			  outline: none;
			  opacity: 0.7;
			  -webkit-transition: .2s;
			  transition: opacity .2s;
			}

			.slider:hover {
			  opacity: 1;
			}

			.slider::-webkit-slider-thumb {
			  -webkit-appearance: none;
			  appearance: none;
			  width: 25px;
			  height: 25px;
			  background: #4CAF50;
			  cursor: pointer;
			}

			.slider::-moz-range-thumb {
			  width: 25px;
			  height: 25px;
			  background: #4CAF50;
			  cursor: pointer;
			}

			#set {
				width:75px;
				height: 25px;
				margin: 0 auto; 
			}
		</style>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>                                                                          
	<script>
		var fatAs = "<?= $fatAs ?>";
		var muscAs = "<?= $muscAs ?>";
		var idAs = "<?= $idAs ?>";
		var style = "<?= $style ?>";

        function choosePic(){
		    let fat = document.getElementById("fat").value;
			let musc = document.getElementById("musc").value;
			console.log("choosing image for " + fat + " fat and " + musc + " musc");
			document.getElementById("vis").setAttribute("src","imgs/"+idAs+"/"+fat+"fat"+musc+"musc<?= $age ?><?= $gender ?>.png");
			console.log({label:fatAs,value:fat});
			report({label:fatAs,value:fat});
			report({label:muscAs,value:musc});
		}

		function setPic(elt){
			//elt.onmousemove=function(evt){}; // stop paying attention to mouse movement
			document.getElementById("fat").disabled=true; // diable both the sliders in case they are visible
			document.getElementById("musc").disabled=true; // diable both the sliders in case they are visible
			report({label:"event", value:"done"});
			document.getElementById("vis").style.border = "2px solid black";
			document.getElementById("set").value="Change";
			document.getElementById("set").onclick = function(){unSet(elt);};
		}

		function unSet(elt){
			//TODO: how do I put the mousemove back on.
			document.getElementById("fat").disabled=false; // diable both the sliders in case they are visible
			document.getElementById("musc").disabled=false; // diable both the sliders in case they are visible
			report({label:"event", value:"undone"});
			document.getElementById("vis").style.border = "none";
			document.getElementById("set").value="Set";
			document.getElementById("set").onclick = function(){setPic(elt);};
		}
		
		function randomiseStart(range){
			let max = parseInt(range.getAttribute("max"));
			let min = parseInt(range.getAttribute("min"));
			let step = parseInt(range.getAttribute("step"));
		    let numberSteps = (max - min) / step + 1;
			let stepItUp = Math.random()*numberSteps;
			range.value = min + step*stepItUp;
			choosePic();
		}
		function mouseMove(pos, searchFor, fullRange){
			if (style=="slider"){
				return;
			}
			let range = document.getElementById(searchFor);
			let max = parseInt(range.getAttribute("max"));
			let min = parseInt(range.getAttribute("min"));
			let step = parseInt(range.getAttribute("step"));
		    let numberSteps = (max - min) / step + 1;
			let jump = Math.floor(fullRange/numberSteps);
			let stepsAcross = Math.floor(pos/jump);
			range.value = stepsAcross*step + min;
			choosePic();
		}
		function report(obj){
            parent.postMessage(obj, "*");
		}
	</script>

	</head>

	<body>
		<input id="fat" 
		       class="slider" 
		       min=<?= $fatMin ?> 
		       max=<?= $fatMax ?> 
		       step=<?= $fatStep ?> 
		       style="display: <?= ($style=='slider')? 'inline' : 'none' ?>"
		       type="range" 
		       onchange="choosePic()" 
		       oninput="choosePic()"/>
		<input id = "set" 
		       type="button" 
		       style="display: <?= ($style=='slider')? 'inline' : 'none' ?>"
		       onclick="setPic(this);" 
		       value="Set">
		<br/>
		<input style="display: none" id="musc" min=<?= $muscMin?> max=<?= $muscMax ?> step=<?= $muscStep?> type="range" onchange="choosePic()" oninput="choosePic()"/>
		<img  onmousemove="mouseMove(event.pageX - this.offsetLeft, 'fat', this.offsetWidth); mouseMove(event.pageY - this.offsetTop,'musc', this.offsetHeight);"
		      ontouchmove="mouseMove(event.pageX - this.offsetLeft, 'fat', this.offsetWidth); mouseMove(event.pageY - this.offsetTop,'musc', this.offsetHeight);"
		      onclick    ="setPic(this);"
		      id         ="vis"
		      style      ="border:2px solid lightgrey"/>
	</body>

	<script>
		console.log("preloading images");
		console.log(($("#fat")));
		console.log(($("#musc")));
		for(var f = parseInt($("#fat").attr("min")); f <=parseInt($("#fat").attr("max")); f += parseInt($("#fat").attr("step"))){
			for(var m = parseInt($("#musc").attr("min")); m <= parseInt($("#musc").attr("max")); m += parseInt($("#musc").attr("step"))){
				let imgString = "imgs/<?= $idAs ?>/"+f+"fat"+m+"musc<?= $age ?><?= $gender ?>.png";
			    console.log("loading file " + imgString);
				$("<img />").attr("src", imgString);
			}
			console.log("finished loading all files")
		}
		//document.getElementById("fat").style.width=document.getElementById("vis").offsetWidth;
		console.log("startup script done");
		$(window).on("load", function() {
			// adjust slider to image width
			document.getElementById("fat").style.width = $("#vis").first().outerWidth()-85;
			// randomise
			console.log("randomising fat");
			randomiseStart(document.getElementById("fat"));
			console.log("randomising musc");
			randomiseStart(document.getElementById("musc"));
		});
	</script>

</html>

